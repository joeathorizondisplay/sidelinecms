<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Nerd;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class NerdController extends Controller
{
    public function index()
    {
        // get all the nerds
        $nerds = Nerd::all();

        // load the view and pass the nerds
        return View::make('nerds.index')
            ->with('nerds', $nerds);
    }

    //
}
