<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::group(['middleware' => ['web']], function () {
Route::resource('tasks', 'TasksController');
Route::get('/', [
    'as' => 'home',
    'uses' => 'PagesController@home'
]);
});

// API routes...
Route::get('/api/v1/products/{id?}', [function($id = null) {
if ($id == null) {
    $products = App\Task::all(array('id', 'title', 'description'));
} else {
    $products = App\Task::find($id, array('id', 'title', 'description'));
}
return Response::json(array(
            'error' => false,
            'products' => $products,
            'status_code' => 200
        ));
}]);

Route::get('/api/v1/categories/{id?}', [function($id = null) {
if ($id == null) {
    $categories = App\Task::all(array('id', 'title'));
} else {
    $categories = App\Task::find($id, array('id', 'title'));
}
return Response::json(array(
            'error' => false,
            'user' => $categories,
            'status_code' => 200
        ));
}]);



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
