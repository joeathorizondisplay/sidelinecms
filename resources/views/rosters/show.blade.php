@extends('layouts.master')

@section('content')
<img src="{{ $rosters->photo }}" alt="{{ $rosters->first_name }}&nbsp{{ $rosters->last_name}}"  height="250">
<h1>{{ camel_case($rosters->first_name) }}&nbsp{{ $rosters->last_name }}</h1>
<h1>#{{ $rosters->jersey }}</h1>

<p class="lead">{{ $rosters->level->name }}</p>
<p class="lead">{{ $rosters->year->name }}</p>
<p class="lead">{{ $rosters->position }}</p>
<p class="lead">{{ $rosters->years_at_sfc }}</p>
<p class="lead">{{ $rosters->height_feet }}'&nbsp{{ $rosters->height_inches }}"</p>
<p class="lead">{{ $rosters->weight }}Lbs.</p>
<p class="lead">{{ $rosters->hometown }}</p>
<p class="lead">{{ $rosters->verse }}</p>
<p class="lead">{{ $rosters->food }}</p>





<hr>

<div class="row">
    <div class="col-md-6">
        <a href="{{ route('rosters.index') }}" class="btn btn-info">Back to all players</a>
        <a href="{{ route('rosters.edit', $rosters->id) }}" class="btn btn-primary">Edit Player</a>
    </div>
    <div class="col-md-6 text-right">
        {!! Form::open([
            'method' => 'DELETE',
            'route' => ['rosters.destroy', $rosters->id]
        ]) !!}
            {!! Form::submit('Delete me??', ['class' => 'btn btn-danger']) !!}
        {!! Form::close() !!}
    </div>
</div>




@stop
