@extends('layouts.master')

@section('content')


<h1>Roster List</h1>
<p class="lead">Here's a list of all your players. <a href="/rosters/create">Add a new one?</a></p>
<hr>


                        <div class="panel panel-primary">
                            <div class="table-responsive">
                                <table class="table table-hover">
                                    <thead  style="background-color:#337AB7; color:white">
                                        <tr>  
                                            <th>  </th>
                                            <th>Jersey</th>                                      
                                            <th>Name</th>
                                            <th>Position</th>
                                            <th>Detail</th>
                                            <th>Edit</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@foreach($rosters as $roster)
                                        <tr>
                                        	<td><img src="{{ $roster->photo }}" alt="{ $roster->first_name }}&nbsp{{ $roster->last_name}}"  height="42"></td>
                                            <td class="ns">{{ $roster->jersey }}</td>
                                            <td>{{ $roster->first_name }}&nbsp{{ $roster->last_name}}</td>
                                            <td>{{ $roster->position}}</td>
                                            <td> <button type="button" class="use-address" data-toggle="modal" data-target="#myModal">Open Modal</button></td>
                                            <td><button type="button" class="use-address" /></td>
                                        </tr>
                                        @endforeach
                               

                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>







 <table id="choose-address-table" class="ui-widget ui-widget-content">
    <thead>
        <tr class="ui-widget-header ">
            <th>Name/Nr.</th>
            <th>Street</th>
            <th>Town</th>
            <th>Postcode</th>
            <th>Country</th>
            <th>Options</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="nr"><span>50</span>
            </td>
            <td class="ns">Some Street 1</td>
            <td>Glasgow</td>
            <td>G0 0XX</td>
            <td>United Kingdom</td>
            <td>
                <button type="button" class="use-address" />
            </td>
        </tr>
        <tr>
            <td class="nr">49</td>
            <td class="ns">Some Street 2</td>
            <td>Glasgow</td>
            <td>G0 0XX</td>
            <td>United Kingdom</td>
            <td>
                <button type="button" class="use-address" />
            </td>
        </tr>
    </tbody>
</table>
  
<input type="text" >

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
                  <div class="row">
            <div class="col-md-6">
				        <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'First Name:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'first_name']) !!}
            </div></div></div>
            <div class="col-md-6">
            <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'Last Name:', ['class' => 'control-label']) !!}
    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
            </div></div></div>
          </div>
                            <div class="row">
            <div class="col-md-6">
				        <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'Jersey:', ['class' => 'control-label']) !!}
    {!! Form::text('jersey', null, ['class' => 'form-control']) !!}
            </div></div></div>
            <div class="col-md-6">
            <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'Position:', ['class' => 'control-label']) !!}
    {!! Form::text('position', null, ['class' => 'form-control']) !!}
            </div></div></div>
          </div>
                            <div class="row">
            <div class="col-md-6">
				        <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'First Name:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'idx']) !!}
            </div></div></div>
            <div class="col-md-6">
            <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'First Name:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'idx']) !!}
            </div></div></div>
          </div>
                            <div class="row">
            <div class="col-md-6">
				        <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'First Name:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'idx']) !!}
            </div></div></div>
            <div class="col-md-6">
            <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'First Name:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'idx']) !!}
            </div></div></div>
          </div>
                            <div class="row">
            <div class="col-md-6">
				        <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'First Name:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'idx']) !!}
            </div></div></div>
            <div class="col-md-6">
            <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'First Name:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'idx']) !!}
            </div></div></div>
          </div>
                            <div class="row">
            <div class="col-md-6">
				        <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'First Name:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'idx']) !!}
            </div></div></div>
            <div class="col-md-6">
            <div class="form-group-sm">
			<div class="col-s-3">
    {!! Form::label('title', 'First Name:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'idx']) !!}
            </div></div></div>
          </div>
        
        <div class="form-group-sm">
			<div class="col-xs-3">

 



    {!! Form::label('title', 'Sport:', ['class' => 'control-label']) !!}
    {{ Form::select('sport_id', $sports), ['class' => 'form-control', 'hidden'=>'true'] }}

    {!! Form::label('title', 'Level:', ['class' => 'control-label']) !!}
     {{ Form::select('level_id', $levels), ['class' => 'form-control'] }}

    {!! Form::label('title', 'Year:', ['class' => 'control-label']) !!}
     {{ Form::select('year_id', $years), ['class' => 'form-control'] }}

    {!! Form::label('title', 'First Name:', ['class' => 'control-label']) !!}
    {!! Form::text('first_name', null, ['class' => 'form-control', 'id'=> 'idx']) !!}








    {!! Form::text('height_inches', null, ['class' => 'form-control']) !!}

    {!! Form::label('title', 'Weight:', ['class' => 'control-label']) !!}
    {!! Form::text('weight', null, ['class' => 'form-control']) !!}

    {!! Form::label('title', 'Hometown:', ['class' => 'control-label']) !!}
    {!! Form::text('hometown', null, ['class' => 'form-control']) !!}

    {!! Form::label('title', 'Years at SFC:', ['class' => 'control-label']) !!}
    {!! Form::text('years_at_sfc', null, ['class' => 'form-control']) !!}

    {!! Form::label('title', 'Verse:', ['class' => 'control-label']) !!}
    {!! Form::text('verse', null, ['class' => 'form-control']) !!}

    {!! Form::label('title', 'Food:', ['class' => 'control-label']) !!}
    {!! Form::text('food', null, ['class' => 'form-control']) !!}

    {!! Form::label('title', 'Photo:', ['class' => 'control-label']) !!}
    {!! Form::text('photo', null, ['class' => 'form-control']) !!}



{!! Form::submit('Create Player', ['class' => 'btn btn-primary']) !!}
<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
{!! Form::close() !!}
</div>
</div>

        
        
        
        
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
   
  
@stop
